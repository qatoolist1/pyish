from pyish.application import Application
candlepin = Application('/home/anchavan/pyprojects/test/app_config.yml')

import json
def get_methods_list_by_model(ap, model):
        list = []
        for method, details in ap.__methods.items():
            if method.startswith(model):
                list.append(method)
        return list

#List All dynamically generated methods
print("================================================================")
count = len(candlepin.__methods)
print("Total Methods = {count}".format(count= count))
print("----------------------------------------------------------------")
print(candlepin.__methods)
print("================================================================")

#List endpoints related to users model
print("================================================================")
model_to_search = 'users'
methods_list = get_methods_list_by_model(ap, model_to_search)
count = len(methods_list)
print("Number of methods for model[{model}] are {count}\n".format(model=model_to_search, count= count))
print(methods_list)
print("================================================================")

#Show details of random method related to users model
print("================================================================")

data = candlepin.__methods['users_get_user_roles']
json_formatted_str = json.dumps(data, indent=2)

print(json_formatted_str)
print("================================================================")

#Call one of the dynamic function from users model
print("-----------------------------------------------------------------")
path_vars = {'username':'RhsmApi_lacisyhP_eohS_metsyS'}
print(candlepin.users_get_user_roles(path_vars))
print("-----------------------------------------------------------------")

#List endpoints related to Consumers model
print("================================================================")
model_to_search = 'consumers'
methods_list = get_methods_list_by_model(ap, model_to_search)
count = len(methods_list)
print("Number of methods for model[{model}] are {count}\n".format(model=model_to_search, count= count))
print(methods_list)
print("================================================================")

#Call one of the dynamic function from Consumers model
print("-----------------------------------------------------------------")
query_params = {'username':'RhsmApi_lacisyhP_eohS_metsyS'}
print(candlepin.consumers_list(None, query_params))
print("-----------------------------------------------------------------")


















