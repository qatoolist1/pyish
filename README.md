# Welcome to Pyish!

A tool, capable of turning a swagger documentation into a reusable client library & a cli application to expedite the workflow & save the time & cost of development, testing & maintenance.

# Sample Configuration File

![sample app config](https://gitlab.com/qatoolist/pyish/-/blob/master/pyish_config.png)

## Create Just In Time Client

JITC - is an object which holds all in-memory references to the API endpoints which you need to invoke.

    from pyish.application import Application
    my_app = Application('<path_to_config_file>/app_config.yml')

Now you can use this object (my_app) to invoke application endpoints

## Introducing a method for debugging purpose

    import json
    def get_methods_list_by_model(my_app, model):
            list = []
            for method, details in my_app.__methods.items():
                if method.startswith(model):
                    list.append(method)
            return list

## Sample Usage

### List All dynamically generated methods

    print("================================================================")
    #used to get count of dynamically injected methods (endpoints)
    count = len(my_app.__methods)
    print("Total Methods = {count}".format(count= count))
    print("----------------------------------------------------------------")
	    
    print(my_app.__methods)
    
    print("================================================================")

### List endpoints related to a particular model (refer swagger)

    print("================================================================")
	# for example I need to know which all methods currospond to users model.
    model_to_search = 'users'
    methods_list = get_methods_list_by_model(my_app, model_to_search)

	# get count of methods which currspond to user model
    count = len(methods_list)

    print("Number of methods for model[{model}] are {count}\n".format(model=model_to_search, count= count))

    print(methods_list)
    print("================================================================")

### Show help on some method related to users model (refer output of above step)

    print("================================================================")
    
    #For instance if we need to invoke get_user_roles endpoint (operationID in swagger)
    #In pyish you refer to any endpoint as follows
    #   <model>_<operationID>
    # for example -
    #   users_get_user_roles
    
    data = my_app.__methods['users_get_user_roles']

    
    json_formatted_str = json.dumps(data, indent=2)
    
    #This will show help text related to that method.
    print(json_formatted_str)
    print("================================================================")

### Let's call the users_get_user_roles endpoint

	#To call any endpoint all you need to pass is the essential parameters which you have seen in help. ( Please refer previous step)
	
    print("-----------------------------------------------------------------")    
    path_vars = {'username':'some_user'}
    print(my_app.users_get_user_roles(path_vars))
    print("-----------------------------------------------------------------")