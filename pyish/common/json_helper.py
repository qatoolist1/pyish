import json
import os

def json_validator(data):
    try:
        json.loads(data)
        return True
    except ValueError as error:
        ###print("invalid json: %s" % error)
        return False

class Endpoint:
        def __init__(self, key):
                self.key = key
                
        def __str__(self):
                return self.key

def change_case(str): 
    res = [str[0].lower()] 
    for c in str[1:]: 
        if c in ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'): 
            res.append('_') 
            res.append(c.lower()) 
        else: 
            res.append(c) 
      
    return ''.join(res) 

def sort_by_tag(collection):
        new_collection = {}
        
        i = 0
        for key, val in collection.items():
                new_dict = {}                
                
                tag = ""
                
                if type(val) == "string".__class__:
                        tag = val
                else:
                        new_dict.update({key:val})
                
                new_collection.update({tag: new_dict})

        ###print(new_collection)

def render_endpoint(method, path, ep):
        
        endpoint_meta = ep.keys()
        
        if 'deprecated' in endpoint_meta:
                #print("returning none {dep}".format(dep = ep['deprecated']))
                return None
        
        endpoint = {}
        
        if not 'operationId' in endpoint_meta:
                ###print(ep)
                pass
        
        operation_key = change_case(ep['operationId'])
        endpoint['path'] = path
        endpoint['method'] = method
        
        if not 'description' in endpoint_meta:
                endpoint['description'] = 'dummy description'
        else:
                endpoint['description'] = ep['description']
                
        endpoint['operation'] = operation_key
        
        tag = ep['tags'][0]
                
        path_vars = {}
        query_params = {}
        headers = {}
        body =  ""
        
        parameters = ep['parameters']
        for p in parameters:
                
                if p['in'] == 'path':
                        path_vars[p['name']] = p['type']
                elif p['in'] == 'query':
                        
                        if p['type'] == 'array':
                                query_params[p['name']] = p['type']+'['+p['items']['type']
                        else:
                                query_params[p['name']] = p['type']
                                
                        if 'collectionFormat' in p:
                                headers['content_type'] = p['collectionFormat']
                        
                elif p['in'] == 'body':
                        body = "dictonary({name})".format(name = p['name'])

        if path.find('{') != -1 and len(path_vars) == 0:
                #print('Missing Path Var')
                pass

        endpoint['parameters'] = {}
        endpoint['parameters']['path_vars'] = path_vars
        endpoint['parameters']['query_params'] = query_params
        endpoint['parameters']['headers'] = headers
        endpoint['parameters']['body'] = body
        endpoint['tag'] = tag
        
        return endpoint
                
def get_yaml(json_file):
        
        paths = json_file['paths']           
        
        i = 0
        tags = {}
        collection = {}
        for key, val in paths.items():                
                path = key
                                
                operations = {}                
                for m, value in val.items():                        
                        endpoint = render_endpoint(m, path, value)                        
                        #print("{path} {method}".format(path = path, method = m))
                        try:
                                if endpoint !=  None:
                                        #print(endpoint['operation'])
                                        operations[Endpoint(endpoint['operation'])] = endpoint
                                        operations['tag'] = endpoint['tag']
                                else: 
                                        #print('endpoint returned none')
                                        pass
                        except:
                             ###print('returning none')
                             return None                          
                                     
                collection.update(operations) 
                
        return collection