from pyish.application import Application

import pyish
import os
import json
import yaml
import inspect

def whoami(inspect_stack):
    name_meta = inspect_stack[len(inspect_stack)-1].code_context[0].split('.')[1]
    n = name_meta.find('(')
    n = len(name_meta) - n
    return name_meta[:-n]

def caller_deligate(self, path_vars=None, query_params=None, 
                    headers=None, body=None, post_params=None, 
                    files=None,response_type=None, auth_settings=None, 
                    async_req=None,_return_http_data_only=None, 
                    collection_formats=None,_preload_content=True, 
                    _request_timeout=None):
        trace = inspect.stack()
        __my_name__ = whoami(trace)
        
        resource_path= self.__methods[__my_name__]['path']
        method = self.__methods[__my_name__]['method'].upper()
        auth_settings = self.configuration.auth_settings()
        response_type = 'JSON'
        
        return self.caller_func(resource_path, method, path_vars, query_params, headers,
                         body, post_params, files, response_type, auth_settings, 
                         async_req, _return_http_data_only, collection_formats, _preload_content, _request_timeout)
        
def load_application_swagger(path_to_json):       
    with open(path_to_json) as file:
        return json.load(file)

def inject_dynamic_methods(cls, endpoints_cfg):
        ###print(type(endpoints_cfg))
        #endpoints_cfg  = json.loads(endpoints_cfg) 
        methods = {}
        
        #print(endpoints_cfg)
        
        for ep, details in endpoints_cfg.items():
                #print(endpoints)
                if not isinstance(details, str):
                        #for endpoint, details in endpoints.items():                                
                        method_name = details['tag']+'_'+str(ep)
                        setattr(cls, method_name, caller_deligate)
                        
                        if 'tag' in details:
                                del details['tag']
                        
                        methods[method_name] = details       
            
        setattr(cls,'__methods', methods)