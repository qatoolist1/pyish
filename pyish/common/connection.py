import requests
from .config import Config
from requests.auth import HTTPBasicAuth

class Connection:
    
    def __init__(self):
        self._config = Config()
    
    def config(self):
        self._config
        
    def get(self, url):
        print("Sending Request with {url} \n {usr} \n {passwd}".format(url = url, usr = self._config._auth['username'], passwd = self._config._auth['password']))
        return requests.get(url, auth=HTTPBasicAuth(self._config._auth['username'], self._config._auth['password']))
    
    def request(self, method, url, expected_status, format = 'JSON', body = None):
        
        if method == 'GET' :
            res = self.get(url)    
        elif method == 'POST' :
            res = self.post(url)
        elif method == 'PUT' :
            res = self.put(url)
            
        if res.status_code != expected_status :
            print("Request unsuccessful! Response Code = {code}".format(code = res.status_code))
            raise "Exception"
    
        if format == 'JSON' :
            return res.json()
        else:
            return res.text()