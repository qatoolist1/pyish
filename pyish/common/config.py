from enum import Enum
import yaml
import multiprocessing
import urllib3

urllib3.disable_warnings()

class Config:
    
    def __init__(self, path_to_config, api_key_generator=None):
        self.load_config(path_to_config)
        self.auth['api_key_generator'] = api_key_generator
    
    def load_config(self, path_to_config):
        
        with open(path_to_config) as file:            
            cfg = yaml.load(file, Loader=yaml.FullLoader)
            #print(cfg)
            
            # Application name for generating the client
            try:
                self.application_name =  cfg[0]['application']['name']
            except KeyError:
                self.application_name = None
                print('Configuration Warning: Application Name not found')
      
            # Application's Swagger.json              
            try:
                self.path_to_swagger_json = cfg[0]['application']['swagger_path']
            except KeyError:
                self.path_to_swagger_json = None
                print('Configuration Warning: Swagger JSON not found')

            # Application's Default Base URL
            try:
                self.base_url = cfg[0]['application']['base_url']
            except KeyError:
                self.base_url = None
                print('Configuration Warning: Base URL not found')
            
            # Authentication Settings
            
            # Dict to store default auth settings
            self.auth = {}
            
            # Auth Type - API_KEY, BASIC_AUTH, 
            try:
                self.auth['type'] = cfg[0]['application']['auth']['type']
            except KeyError:
                self.auth['type'] = None
                print('Configuration Warning: Auth Type not found')
            
            # Username for HTTP basic authentication
            try:
                self.auth['username'] = cfg[0]['application']['auth']['username']
            except KeyError:
                self.auth['username'] = None
                print('Configuration Warning: User Name not found')
            
            # Password for HTTP basic authentication
            try:
                self.auth['password'] = cfg[0]['application']['auth']['password']
            except KeyError:
                self.auth['password'] = None
                print('Configuration Warning: Password not found')
            
            # API_KEY for Key based authentication
            try:
                self.auth['api_key'] = cfg[0]['application']['auth']['api_key']
            except KeyError:
                self.auth['api_key'] = None
                print('Configuration Warning: API Key not found')
            
            # function to refresh API key if expired, Initially set to none but the exposed method could be used to initialize
            self.auth['api_key_generator'] = None
                        
            # API key prefix (e.g. Bearer)
            try:
                self.auth['api_key_prefix'] = cfg[0]['application']['auth']['api_key_prefix']
            except KeyError:
                self.auth['api_key_prefix'] = ''
                print('Configuration Warning: API Key Prefix not found')
                        
            # SSL/TLS verification
            # Set this to false to skip verifying SSL certificate when calling API
            # from https server.
            try:
                self.verify_ssl = cfg[0]['application']['verify_ssl']
            except KeyError:
                self.verify_ssl = False
                
            self.client_certs = {}
            
            # Set this to customize the certificate file to verify the peer.
            try:
                self.client_certs['ssl_ca_cert'] = cfg[0]['application']['client_certs']['ssl_ca_cert']
            except KeyError:
                self.client_certs['ssl_ca_cert'] = None
                print('Configuration Warning: PEM Path not found')
            
            # client certificate file [.crt]
            try:
                self.client_certs['cert_file'] = cfg[0]['application']['client_certs']['cert_file']
            except KeyError:
                self.client_certs['cert_file'] = None
                print('Configuration Warning: API Key Prefix not found')
            
            # client key file [.key]
            try:
                self.client_certs['key'] = cfg[0]['application']['client_certs']['key_file']
            except KeyError:
                self.client_certs['key'] = None
                print('Configuration Warning: API Key Prefix not found')
                        
            # urllib3 connection pool's maximum number of connections saved
            # per pool. urllib3 uses 1 connection as default value, but this is
            # not the best value when you are making a lot of possibly parallel
            # requests to the same host, which is often the case here.
            # cpu_count * 5 is used as default value to increase performance.            
            try:
                self.connection_pool_maxsize = cfg[0]['application']['connection_pool_maxsize']
            except KeyError:
                self.connection_pool_maxsize = multiprocessing.cpu_count() * 5
                print("Configuration Warning: connection_pool_maxsize not found assigning default to {pool_max}".format(pool_max= self.connection_pool_maxsize))
        
            # Proxy URL
            try:
                self.proxy = cfg[0]['application']['proxy']
            except KeyError:
                self.proxy = None
                print('Configuration Warning: Proxy not found')
                
            # Set this to True/False to enable/disable SSL hostname verification.
            self.assert_hostname = None

            # Safe chars for path_param
            self.safe_chars_for_path_param = ''

    def get_api_key_with_prefix(self):
        """Gets API key (with prefix if set).

        :param identifier: The identifier of apiKey.
        :return: The token for api key authentication.
        """

        if self.auth['api_key_generator']:
            self.auth['api_key_generator'](self)
        
        try:
            key = self.auth['api_key']
            if key:
                prefix = self.auth['api_key_generator']
                if prefix:
                    return "%s %s" % (prefix, key)
                else:
                    return key
        except KeyError:
            key = None

    def get_basic_auth_token(self):
        """Gets HTTP basic authentication header (string).

        :return: The token for basic HTTP authentication.
        """
        return urllib3.util.make_headers(
            basic_auth = self.auth['username'] + ':' + self.auth['password']
        ).get('authorization')

    def auth_settings(self):
        """Gets Auth Settings dict for api client.

        :return: The Auth Settings information dict.
        """
        
        token = self.get_basic_auth_token()
        
        return {
            'basic':
                {
                    'type': 'basic',
                    'in': 'header',
                    'key': 'Authorization',
                    'value': token
                },

        }

    def set_api_key_generator(self, fn):
        self.auth['api_key_generator'] = fn