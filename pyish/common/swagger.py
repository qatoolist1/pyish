import json
import os

#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#contactObject
class Contact(object):
        def __init__(self, name, url, email):
                self.name = name
                self.url = url
                self.email = email
                
        def __str__(self):
                return "Contact Details:\n    Name : {name}\n    URL: {url}\n    Email : {email}\n".format(name = self.name, url = self.url, email = self.email)

#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#licenseObject
class License(object):
        def __init__(self, name=None, url=None):
                #According to OpenAPI specifications, the name is a required field
                self.name = name
                self.url = url
                
        def __str__(self):
                return "License Details:\n    Name : {name}\n    URL: {url}\n".format(name = self.name, url = self.url)

#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#info-object
class Info(object):
        def __init__(self, title=None, description=None, terms_of_service=None, contact = None, license = None, version = None):
                self.title = title
                self.description = description
                self.terms_of_service= terms_of_service
                self.contact = contact
                self.license = license
                self.version = version
                
        def __str__(self):
                string_template = "Info:\n  Title : {title}\n  Description : {description}\n  Terms of service : {tos}\n  {contact}  {license}  Version : {version}"
                
                return string_template.format(title = self.title, 
                                              description  = self.description, 
                                              tos = self.terms_of_service, 
                                              contact = str(contact))


#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#schemaObject
class ExternalDocumentObject(object):
        def __init__(self, description=None, url=None):
                self.description = description
                self.url = url
        
        def __str__(self):
                return "External Document Details:\n    Description : {description}\n    URL: {url}\n".format(description = self.description, url = self.url) 

#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#items-object
class Items(object):
        def __init__(self, type, format, items, collection_format, default, maximum, exclusive_minimum, 
                     minimum, exclusive_maximum, max_length, min_length, pattern, max_items, min_items, 
                     unique_items, enum, multiple_of):
                self.type = type
                self.format = format
                self.items = items
                self.collection_format = collection_format
                self.default = default
                self.maximum = maximum
                self.exclusive_minimum = exclusive_minimum
                self.minimum = minimum
                self.exclusive_maximum = exclusive_maximum
                self.max_length = max_length
                self.min_length = min_length
                self.pattern = pattern
                self.max_items = max_items
                self.min_items = min_items
                self.unique_items = unique_items
                self.enum = enum
                self.multiple_of = multiple_of

        def __str__(self):
                return "Items:\n Type : ${type}\n  Format : ${format}\n  Items : ${items}\n  Collection Format : ${collection_format}\n  Default : ${default}\n  Maximum : ${maximum}\n  Exclusive Minimum : ${exclusive_minimum}\n  Minimum : ${minimum}\n  Exclusive Maximum : ${exclusive_maximum}\n  Max Length : ${max_length}\n  Min Length : ${min_length}\n  Pattern : ${pattern}\n  Max Items : ${max_items}\n  Min Items : ${min_items}\n  Unique Items : ${unique_items}\n  Enum : ${enum}\n  Multiple Of : ${multiple_of}\n".format(type = self.type, format = self.format, items = self.items, collection_format = self.collection_format, default = self.default, maximum = self.maximum, exclusive_minimum = self.exclusive_minimum, minimum = self.minimum, exclusive_maximum = self.exclusive_maximum, max_length = self.max_length, min_length = self.min_length, pattern = self.pattern, max_items = self.max_items, min_items = self.min_items, unique_items = self.unique_items, enum = self.enum, multiple_of = self.multiple_of)

#https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#schemaObject
class BodySchema(object):
        def __init__(self, ref, format, title, description, default, multiple_of, maximum, exclusive_minimum, 
                     minimum, exclusive_maximum, max_length, min_length, pattern, max_items, min_items, 
                     unique_items, max_properties, required, enum, type, items, all_of, properties, additional_properties, 
                     discriminator = None, readOnly = False, xml = None, external_docs = None, example = None):
                self.ref = ref
                self.format = format
                self.title = title
                self.description = description
                self.default = default
                self.multiple_of = multiple_of
                self.maximum = maximum
                self.exclusive_minimum = exclusive_minimum
                self.minimum = minimum
                self.exclusive_maximum = exclusive_maximum
                self.max_length = max_length
                self.min_length = min_length
                self.pattern = pattern
                self.max_items = max_items
                self.min_items = min_items
                self.unique_items = unique_items
                self.max_properties = max_properties
                self.required = required
                self.enum = enum
                self.type = type
                self.items = items
                self.all_of = all_of
                self.properties = properties
                self.additional_properties = additional_properties
                self.discriminator  = discriminator 
                self.readOnly  = readOnly 
                self.xml  = xml 
                self.external_docs  = external_docs 
                self.example = example

        def __str__(self):
                return "Body:\n  Ref : ${ref}\n  Format : ${format}\n  Title : ${title}\n  Description : ${description}\n  Default : ${default}\n  Multiple Of : ${multiple_of}\n  Maximum : ${maximum}\n  Exclusive Minimum : ${exclusive_minimum}\n  Minimum : ${minimum}\n  Exclusive Maximum : ${exclusive_maximum}\n  Max Length : ${max_length}\n  Min Length : ${min_length}\n  Pattern : ${pattern}\n  Max Items : ${max_items}\n  Min Items : ${min_items}\n  Unique Items : ${unique_items}\n  Max Properties : ${max_properties}\n  Required : ${required}\n  Enum : ${enum}\n  Type : ${type}\n  Items : ${items}\n  All Of : ${all_of}\n  Properties : ${properties}\n  Additional Properties : ${additional_properties}\n  Discriminator  : ${discriminator }\n  Readonly  : ${readOnly }\n  Xml  : ${xml }\n  External Docs  : ${external_docs }\n  Example : ${example}\n".format(ref = self.ref, format = self.format, title = self.title, description = self.description, default = self.default, multiple_of = self.multiple_of, maximum = self.maximum, exclusive_minimum = self.exclusive_minimum, minimum = self.minimum, exclusive_maximum = self.exclusive_maximum, max_length = self.max_length, min_length = self.min_length, pattern = self.pattern, max_items = self.max_items, min_items = self.min_items, unique_items = self.unique_items, max_properties = self.max_properties, required = self.required, enum = self.enum, type = self.type, items = self.items, all_of = self.all_of, properties = self.properties, additional_properties = self.additional_properties, discriminator  = self.discriminator , readOnly  = self.readOnly , xml  = self.xml , external_docs  = self.external_docs , example = self.example)

class Parameter(object):
        def __init__(self, name, _in, description, required, schema = None, 
                     type = None, format = None, allow_empty_value = None,
                     items = None, collection_format=None, default = None, 
                     maximum = None, exclusive_maximum = None, minimum = None, 
                     exclusive_minimum = None, max_length = None, min_length = None, 
                     pattern = None, max_items = None, min_items = None, unique_items = None,
                     enum = None, multiple_of = None):
                pass
        
        def __str__(self):
                
                if self._in == 'body':
                        return "Parameter Details:\n   Name : ${name}\n   In : ${_in}\n  Description : ${description}\n  Required : ${required}\n  Schema : ${schema}".format(name = self.name, _in = self._in, description = self.description, required = self.required, schema = str(self.schema))
                
                return "Parameter Details:\n   Name : ${name}\n   In : ${_in}\n  Description : ${description}\n  Required : ${required}\n  Schema : ${schema}\n  Type : ${type}\n  Format : ${format}\n  Allow Empty Value : ${allow_empty_value}\n  Items  : ${items }\n  Collection Format : ${collection_format}\n  Default : ${default}\n  Maximum : ${maximum}\n  Exclusive Maximum : ${exclusive_maximum}\n  Minimum : ${minimum}\n   exclusive Minimum : ${ exclusive_minimum}\n  Max Length : ${max_length}\n  Min Length : ${min_length}\n  Pattern : ${pattern}\n  Max Items : ${max_items}\n  Min Items : ${min_items}\n  Unique Items : ${unique_items}\n  Enum  : ${enum }\n  Multiple Of : ${multiple_of}\n".format(name = self.name, _in = self._in, description = self.description, required = self.required, schema = self.schema, type = self.type, format = self.format, allow_empty_value = self.allow_empty_value, items  = self.items , collection_format = self.collection_format, default = self.default, maximum = self.maximum, exclusive_maximum = self.exclusive_maximum, minimum = self.minimum,  exclusive_minimum = self. exclusive_minimum, max_length = self.max_length, min_length = self.min_length, pattern = self.pattern, max_items = self.max_items, min_items = self.min_items, unique_items = self.unique_items, enum  = self.enum , multiple_of = self.multiple_of)
                
#According to OpenAPI Spec responses is required field for any operation
class Operation(object):
        def __init__(self, tags, summary, description, external_docs, operation_id, consumes, 
                     produces, parameters, responses, schemas, depricated, security):
                self.tags = tags
                self.summary = summary
                self.description = description
                self.external_docs = external_docs
                self.operation_id = operation_id
                self.consumes = consumes
                self.produces = produces
                self.parameters = parameters
                self.responses = responses
                self.schemas = schemas
                self.depricated = depricated
                self.security = security
        
        def __str__(self):
                return "Tags : ${tags}\n  Summary : ${summary}\n  Description : ${description}\n  External Docs : ${external_docs}\n  Operation Id : ${operation_id}\n  Consumes : ${consumes}\n  Produces : ${produces}\n  Parameters : ${parameters}\n  Responses : ${responses}\n  Schemas : ${schemas}\n  Depricated : ${depricated}\n  Security : ${security}\n".format(tags = self.tags, summary = self.summary, description = self.description, external_docs = self.external_docs, operation_id = self.operation_id, consumes = self.consumes, produces = self.produces, parameters = self.parameters, responses = self.responses, schemas = self.schemas, depricated = self.depricated, security = self.security)

def json_validator(data):
    try:
        json.loads(data)
        return True
    except ValueError as error:
        print("invalid json: %s" % error)
        return False

def load():
    __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))        
    swag_path = os.path.join(__location__, 'swagger.json')        
    with open(swag_path) as file:
        return json.load(file)

def change_case(str): 
    res = [str[0].lower()] 
    for c in str[1:]: 
        if c in ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'): 
            res.append('_') 
            res.append(c.lower()) 
        else: 
            res.append(c) 
      
    return ''.join(res) 

def sort_by_tag(collection):
        new_collection = {}
        
        i = 0
        for key, val in collection.items():
                new_dict = {}                
                
                tag = ""
                
                if type(val) == "string".__class__:
                        tag = val
                else:
                        new_dict.update({key:val})
                
                new_collection.update({tag: new_dict})

        print(new_collection)

def render_endpoint(method, path, ep):
        
        endpoint_meta = ep.keys()
        
        if 'deprecated' in endpoint_meta:
                #print("returning none {dep}".format(dep = ep['deprecated']))
                return None
        
        endpoint = {}
        
        if not 'operationId' in endpoint_meta:
                print(ep)
        
        operation_key = change_case(ep['operationId'])
        endpoint['path'] = path
        endpoint['method'] = method
        
        if not 'description' in endpoint_meta:
                endpoint['description'] = 'dummy description'
        else:
                endpoint['description'] = ep['description']
                
        endpoint['operation'] = operation_key
        
        tag = ep['tags'][0]
                
        path_vars = {}
        query_params = {}
        headers = {}
        body =  ""
        
        parameters = ep['parameters']
        for p in parameters:
                
                if p['in'] == 'path':
                        path_vars[p['name']] = p['type']
                elif p['in'] == 'query':
                        
                        if p['type'] == 'array':
                                query_params[p['name']] = p['type']+'['+p['items']['type']
                        else:
                                query_params[p['name']] = p['type']
                                
                        if 'collectionFormat' in p:
                                headers['content_type'] = p['collectionFormat']
                        
                elif p['in'] == 'body':
                        body = "dictonary({name})".format(name = p['name'])

        if path.find('{') != -1 and len(path_vars) == 0:
                #print('Missing Path Var')
                pass

        endpoint['parameters'] = {}
        endpoint['parameters']['path_vars'] = path_vars
        endpoint['parameters']['query_params'] = query_params
        endpoint['parameters']['headers'] = headers
        endpoint['parameters']['body'] = body
        endpoint['tag'] = tag
        
        return endpoint
        
        
def iterate():
        
        json_file = load()
        paths = json_file['paths']           
        
        i = 0
        tags = {}
        collection = {}
        for key, val in paths.items():                
                path = key
                                
                operations = {}                
                for m, value in val.items():                        
                        endpoint = render_endpoint(m, path, value)                        
                        #print("{path} {method}".format(path = path, method = m))
                        try:
                                if endpoint !=  None:
                                        #print(endpoint['operation'])
                                        operations[endpoint['operation']] = endpoint
                                        operations['tag'] = endpoint['tag']
                                else: 
                                        #print('endpoint returned none')
                                        pass
                        except:
                             print('returning none')
                             return None                          
                                     
                collection.update(operations) 
                
        return collection
#                if 'tag' in collection.keys():
#                        
#                        print(collection.keys())
#                        tags[collection['tag']] = collection
#                else:
#                        print(operations)
#                        print(path) 
                                
                
        
        #res_bytes = json.dumps(tags).encode('utf-8') 
        ##print(json.dumps(tags))
        #print(json_validator(res_bytes))
        
                
        #print(json.dumps(tags))
        #res_bytes = json.dumps(tags).encode('utf-8') 
        #print(json_validator(res_bytes))