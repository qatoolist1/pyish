from pyish.common import Configuration, get_yaml
import pyish.common.helpers as helpers
from pyish.common.client import Client

class Application(Client):

    def __init__(self, config_path, api_key_generator= None):
        config = Configuration(config_path, api_key_generator)
        swagger_path = config.path_to_swagger_json
        super(self.__class__, self).__init__(config)
            
        self._endpoints_cfg = helpers.load_application_swagger(swagger_path)                    
        self.__yaml_representation = get_yaml(self._endpoints_cfg)
        
        ###print(self.__yaml_representation)
        
        helpers.inject_dynamic_methods(Application, self.__yaml_representation)  

    def endpoints(self):
        return self._endpoints
    
    