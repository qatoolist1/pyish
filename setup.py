from distutils.core import setup

setup(
    name='pyish',
    version='0.1dev',
    packages=['pyish',],
    license='MIT',
    long_description=open('README.txt').read(),
)
